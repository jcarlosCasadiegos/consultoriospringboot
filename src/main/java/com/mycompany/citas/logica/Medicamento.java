/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.citas.logica;

//import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author USUARIO
 */
@Component("medicamento")
public class Medicamento {
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    private int id;
    private String nombre;
    private String presentacion;
    private String descripcion;
    private String fabricante;

    public Medicamento(int id, String nombre, String presentacion, String descripcion, String fabricante) {
        this.id = id;
        this.nombre = nombre;
        this.presentacion = presentacion;
        this.descripcion = descripcion;
        this.fabricante = fabricante;
    }
    
    public Medicamento(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }
    
    //CRUD guardar
    public String guardarMedicamento() throws ClassNotFoundException, SQLException{
        
        String sql = "INSERT INTO medicamentos(nombre,presentacion,descripcion,fabricante) VALUES(?,?,?,?)";
        return sql;
    }
    
    //Consultar
    public boolean consultarMedicamento() throws ClassNotFoundException, SQLException{
        
        String sql = "SELECT id,nombre,presentacion,descripcion,fabricante FROM medicamentos WHERE id= ?";
        List<Medicamento> medi = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Medicamento(
                        rs.getInt("id"), //1007
                        rs.getString("nombre"),
                        rs.getString("presentacion"),
                        rs.getString("descripcion"),
                        rs.getString("fabricante")                        
                ), new Object[]{this.getId()});
        if(medi != null && medi.size() > 0){
            this.setId(medi.get(0).getId());
            this.setNombre(medi.get(0).getNombre());
            this.setPresentacion(medi.get(0).getPresentacion());
            this.setDescripcion(medi.get(0).getDescripcion());
            this.setFabricante(medi.get(0).getFabricante());
            return true;
        }
        else{
            return false;
        }
        
    }
    
    //Actualizar
    public String  actualizarMedicamento() throws ClassNotFoundException, SQLException{
        
        String sql = "UPDATE medicamentos SET id = ?, nombre = ?, presentacion = ?, descripcion = ?, fabricante = ? WHERE id = ?";
        return sql;
        
    }
    
    //Borrar
    public String borrarMedicamento() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM medicamentos WHERE id= ? ";
        return sql;
    }

    @Override
    public String toString() {
        return "Medicamento{" + "id=" + id + ", nombre=" + nombre + ", presentacion=" + presentacion + ", descripcion=" + descripcion + ", fabricante=" + fabricante + '}';
    }
    
    
}
