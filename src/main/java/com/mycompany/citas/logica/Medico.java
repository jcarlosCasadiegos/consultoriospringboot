/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.citas.logica;

//import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author USUARIO
 */
@Component("medico")
public class Medico extends Persona{
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    //Atributos
    private String profesion;
    private double sueldo;

    public Medico(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion, String usuario, String contrasenia, String profesion, double sueldo, String tipoUsuario) {
        super(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia, tipoUsuario);
        this.profesion = profesion;
        this.sueldo = sueldo;
    }

    public Medico(){}

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    //CRUD guardar
    public String guardarMedico() throws ClassNotFoundException, SQLException{
        
        String sql = "INSERT INTO medicos(cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,profesion,salario,tipoUsuario)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        return sql;
    }
    
    //Consultar
    public boolean consultarAdministrador() throws ClassNotFoundException, SQLException{
        
        String sql = "SELECT cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,profesion,salario,tipoUsuario FROM medicos WHERE cedula= ?";
        List<Medico> medico = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Medico(
                        rs.getString("cedula"), //1007
                        rs.getString("nombre"),
                        rs.getString("apellido"),
                        rs.getInt("edad"),
                        rs.getString("telefono"),
                        rs.getString("correo"),
                        rs.getString("direccion"),
                        rs.getString("usuario"),
                        rs.getString("contrasenia"),
                        rs.getString("profesion"),
                        rs.getDouble("salario"),
                        rs.getString("tipoUsuario")                        
                ), new Object[]{this.getCedula()});
        if(medico != null && medico.size() > 0){
            this.setCedula(medico.get(0).getCedula());
            this.setNombre(medico.get(0).getNombre());
            this.setApellido(medico.get(0).getApellido());
            this.setEdad(medico.get(0).getEdad());
            this.setTelefono(medico.get(0).getTelefono());
            this.setCorreo(medico.get(0).getCorreo());
            this.setDireccion(medico.get(0).getDireccion());
            this.setUsuario(medico.get(0).getUsuario());
            this.setContrasenia(medico.get(0).getContrasenia());
            this.setProfesion(medico.get(0).getProfesion());
            this.setSueldo(medico.get(0).getSueldo());
            this.setTipoUsuario(medico.get(0).getTipoUsuario());
            return true;
        }
        else{
            return false;
        }
        
    }
    
    //Actualizar
    public String  actualizarMedico() throws ClassNotFoundException, SQLException{
        
        String sql = "UPDATE medicos SET cedula = ?, nombre = ?, apellido = ?, edad = ?, telefono = ?, correo = ?, direccion = ?, usuario = ?, contrasenia = ?, profesion= ?, salario= ?, tipoUsuario = ? WHERE cedula = ?";
        return sql;
        
    }
    
    //Borrar
    public String borrarMedico() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM medicos WHERE cedula= ? ";
        return sql;
    }
    
    @Override
    public String toString() {
        return "Medico{" + "cedula=" + getCedula() + ", nombre=" + getNombre() + ", apellido=" + getApellido() +
                ", edad=" + getEdad() + ", telefono=" + getTelefono() + ", correo=" + getCorreo() + 
                ", direccion=" + getDireccion() + ", usuario=" + getUsuario() + ", contrasenia=" + getContrasenia() +
                ", proesion=" + profesion + ", salario=" + sueldo + ", tipoUsuario=" + getTipoUsuario() + '}';
        //return "Medico{" + "profesion=" + profesion + ", sueldo=" + sueldo + '}';
    }
   
    
}
