/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.citas.logica;

//import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author USUARIO
 */
@Component("cita")
public class Cita {
    
    @Autowired
    JdbcTemplate jdbctemplate;
    
    private int id;
    private String tipoCita;
    private String fechaRegistro;
    private String fechaCita;
    private String pacienteCedula;

    public Cita(int id, String tipoCita, String fechaRegistro, String fechaCita, String pacienteCedula) {
        this.id = id;
        this.tipoCita = tipoCita;
        this.fechaRegistro = fechaRegistro;
        this.fechaCita = fechaCita;
        this.pacienteCedula = pacienteCedula;
    }
    
    public Cita(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipoCita() {
        return tipoCita;
    }

    public void setTipoCita(String tipoCita) {
        this.tipoCita = tipoCita;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getPacienteCedula() {
        return pacienteCedula;
    }

    public void setPacienteCedula(String pacienteCedula) {
        this.pacienteCedula = pacienteCedula;
    }
    
    //Guardar
    public String guardarCita() throws ClassNotFoundException, SQLException{
        
        String sql = "INSERT INTO citas(tipoCita,fechaRegistro,fechaCita,pacienteCedula) VALUES(?,?,?,?)";
        return sql;
    }
    
    public boolean consultarCita() throws ClassNotFoundException, SQLException{
        String sql = "SELECT id,tipoCita,fechaRegistro,fechaCita,pacienteCedula FROM citas WHERE pacienteCedula= ? ";
        
        List<Cita> citas = jdbctemplate.query(sql, (rs, rowNum)
                -> new Cita(
                        rs.getInt("id"),
                        rs.getString("tipoCita"),
                        rs.getString("fechaRegistro"),
                        rs.getString("fechaCita"),
                        rs.getString("pacienteCedula")
                ), new Object[]{this.getPacienteCedula()});
        if(citas != null && citas.size() > 0){
            this.setId(citas.get(0).getId());
            this.setTipoCita(citas.get(0).getTipoCita());
            this.setFechaRegistro(citas.get(0).getFechaRegistro());
            this.setFechaCita(citas.get(0).getFechaCita());
            this.setPacienteCedula(citas.get(0).getPacienteCedula());
            
            return true;
        }
        else{
            return false;
        }
         
    }
    
    //Esta actualizacion hay que validarla para que solo la haga el administrador.
    
    public String actualizarCita() throws ClassNotFoundException, SQLException{
        String sql = "UPDATE citas SET tipoCita= ?, fechaCita= ?";
        return sql;
    }
    
    public String borrarCita() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM citas WHERE id= ?";
        return sql;     
    }

    @Override
    public String toString() {
        return "Cita{" + "id=" + id + ", tipoCita=" + tipoCita + ", fechaRegistro=" + fechaRegistro + ", fechaCita=" + fechaCita + ", pacienteCedula=" + pacienteCedula + '}';
    }
    
    
}
