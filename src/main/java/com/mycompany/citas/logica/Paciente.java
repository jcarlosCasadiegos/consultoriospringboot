/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.citas.logica;

//import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author USUARIO
 */
@Component("paciente")
public class Paciente extends Persona{

    @Autowired
    JdbcTemplate jdbcTemplate;
    
    public Paciente(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion, String tipoUsuario, String usuario, String contrasenia) {
        super(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia, tipoUsuario);
    }
    
    public Paciente(){}
    
    //CRUD guardar
    public String guardarPaciente() throws ClassNotFoundException, SQLException{
        
        String sql = "INSERT INTO pacientes(cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,tipoUsuario)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?)";
        return sql;
    }
    
    //Consultar
    public boolean consultarPaciente() throws ClassNotFoundException, SQLException{
        
        String sql = "SELECT cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,tipoUsuario FROM pacientes WHERE cedula= ?";
        List<Paciente> paci = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Paciente(
                        rs.getString("cedula"), //1007
                        rs.getString("nombre"),
                        rs.getString("apellido"),
                        rs.getInt("edad"),
                        rs.getString("telefono"),
                        rs.getString("correo"),
                        rs.getString("direccion"),
                        rs.getString("usuario"),
                        rs.getString("contrasenia"),
                        rs.getString("tipoUsuario")                        
                ), new Object[]{this.getCedula()});
        if(paci != null && paci.size() > 0){
            this.setCedula(paci.get(0).getCedula());
            this.setNombre(paci.get(0).getNombre());
            this.setApellido(paci.get(0).getApellido());
            this.setEdad(paci.get(0).getEdad());
            this.setTelefono(paci.get(0).getTelefono());
            this.setCorreo(paci.get(0).getCorreo());
            this.setDireccion(paci.get(0).getDireccion());
            this.setUsuario(paci.get(0).getUsuario());
            this.setContrasenia(paci.get(0).getContrasenia());
            this.setTipoUsuario(paci.get(0).getTipoUsuario());
            return true;
        }
        else{
            return false;
        }
        
    }
    
    //Actualizar
    public String  actualizarPaciente() throws ClassNotFoundException, SQLException{
        
        String sql = "UPDATE pacientes SET cedula = ?, nombre = ?, apellido = ?, edad = ?, telefono = ?, correo = ?, direccion = ?, usuario = ?, contrasenia = ?, tipoUsuario = ? WHERE cedula = ?";
        return sql;
        
    }
    
    //Borrar
    public String borrarPaciente() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM pacientes WHERE cedula= ? ";
        return sql;
    }
    
    @Override
    public String toString() {
        return "Paciente{" + "cedula=" + getCedula() + ", nombre=" + getNombre() + ", apellido=" + getApellido() +
                ", edad=" + getEdad() + ", telefono=" + getTelefono() + ", correo=" + getCorreo() + 
                ", direccion=" + getDireccion() + ", usuario=" + getUsuario() + ", contrasenia=" + getContrasenia() +
                ", tipoUsuario=" + getTipoUsuario() + '}';
        //return "Medico{" + "profesion=" + profesion + ", sueldo=" + sueldo + '}';
    }
    
    
}
