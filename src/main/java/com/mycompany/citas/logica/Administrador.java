/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.citas.logica;

//import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author USUARIO
 */

@Component("administrador") //Hay que convertirlo en componentes y dejan de ser modelos.
public class Administrador extends Persona{
    
    @Autowired
    JdbcTemplate jdbcTemplate;       
    
    public Administrador(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion, String tipoUsuario, String usuario, String contrasenia) {
        super(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia, tipoUsuario);
    }
    
    public Administrador(){}
    
    //CRUD guardar
    public String guardarAdministrador() throws ClassNotFoundException, SQLException{
        
        String sql = "INSERT INTO administradores(cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,tipoUsuario)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?)";
        return sql;
    }
    
    //Consultar
    public boolean consultarAdministrador() throws ClassNotFoundException, SQLException{
        
        //String sql = "SELECT cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia etc FROM administradores WHERE cedula = ?";
        //return sql;
        
        String sql = "SELECT cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,tipoUsuario FROM administradores WHERE cedula= ?";
        List<Administrador> admin = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Administrador(
                        rs.getString("cedula"), //1007
                        rs.getString("nombre"),
                        rs.getString("apellido"),
                        rs.getInt("edad"),
                        rs.getString("telefono"),
                        rs.getString("correo"),
                        rs.getString("direccion"),
                        rs.getString("usuario"),
                        rs.getString("contrasenia"),
                        rs.getString("tipoUsuario")                        
                ), new Object[]{this.getCedula()});
        if(admin != null && admin.size() > 0){
            this.setCedula(admin.get(0).getCedula());
            this.setNombre(admin.get(0).getNombre());
            this.setApellido(admin.get(0).getApellido());
            this.setEdad(admin.get(0).getEdad());
            this.setTelefono(admin.get(0).getTelefono());
            this.setCorreo(admin.get(0).getCorreo());
            this.setDireccion(admin.get(0).getDireccion());
            this.setUsuario(admin.get(0).getUsuario());
            this.setContrasenia(admin.get(0).getContrasenia());
            this.setTipoUsuario(admin.get(0).getTipoUsuario());
            return true;
        }
        else{
            return false;
        }
        
    }
    
    //Actualizar
    public String  actualizarAdministrador() throws ClassNotFoundException, SQLException{
        
        String sql = "UPDATE administradores SET cedula = ?, nombre = ?, apellido = ?, edad = ?, telefono = ?, correo = ?, direccion = ?, usuario = ?, " +
                ", contrasenia = ?, tipoUsuario = ? WHERE cedula = ?";
        return sql;
        
    }
    
    //Borrar
    public String borrarAdministrador() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM administradores WHERE cedula= ? ";
        return sql;
    }
    
    @Override
    public String toString() {
        return "Administrador{" + "cedula=" + getCedula() + ", nombre=" + getNombre() + ", apellido=" + getApellido() +
                ", edad=" + getEdad() + ", telefono=" + getTelefono() + ", correo=" + getCorreo() + 
                ", direccion=" + getDireccion() + ", usuario=" + getUsuario() + ", contrasenia=" + getContrasenia() +
                ", tipoUsuario=" + getTipoUsuario() + '}';
        //return "Medico{" + "profesion=" + profesion + ", sueldo=" + sueldo + '}';
    }
    
}
