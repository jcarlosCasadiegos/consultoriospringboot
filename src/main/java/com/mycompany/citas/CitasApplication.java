package com.mycompany.citas;

import com.mycompany.citas.logica.Cita;
import com.mycompany.citas.logica.Paciente;
import java.sql.SQLException;
import net.bytebuddy.implementation.bytecode.constant.DefaultValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication //Donde se encuentra este arroba indica la pagina principal main.
@RestController //Indica que la clase será un API REST.
public class CitasApplication {
        @Autowired //Perimte la conexión de jdbc template //Nos ahorramos el new a la hora de instanciar un objeto o una clase.
        Cita cita;
        
        @Autowired
        Paciente paciente;
        //JdbcTemplate jdbcTemplate; //Instancia del jdbctemplate el cual es el equivalente al statement de la clase conexión.
        
        /*
        Ventajas:
        Manejo de excepciones controlados
        Evita el sqlInjection para no permitir ataques de Hackers.
        */
	public static void main(String[] args) {
		SpringApplication.run(CitasApplication.class, args);
	}
        
        //Para trabajar los API de las paginas web, acá vamos a tener todas las vistas de la aplicación.
        @GetMapping("/hello")
        public String hello(@RequestParam(value="name", defaultValue = "World") String name, @RequestParam(value= "edad", defaultValue = "24") String edad){
            
            return String.format("Hello %s, you are %s years old! ", name, edad);
            
        }
           
        @GetMapping("/consultarCitas")
        public String consultarCitas(@RequestParam(value = "cedula", defaultValue = "1") String cedula) throws ClassNotFoundException, SQLException{
            paciente.setCedula(cedula);
            if(paciente.consultarPaciente()){
                cita.setPacienteCedula(cedula);
                if(cita.consultarCita()){
                    return "(\"id\":\""+cita.getId()+"\",\"tipoCita\":\""+cita.getTipoCita()+"\",\"fechaRegistro\":\""+cita.getFechaRegistro()+"\",\"fechaCita\":\""+cita.getFechaCita()+
                            "\",\"pacienteCedula\":\""+cita.getPacienteCedula()+"\")"; //Esto es una estructura de angular.
                }
                else{
                    return "(\"id\":\""+cedula+"\",\"cedulaPaciente\":\""+"El paciente no tiene ninguna cita registrada"+"\")";
                }
            }
            else{
                return "(\"Paciente no se encuentra registrado\")";
            }
        }
}
